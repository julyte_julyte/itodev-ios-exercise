//
//  SectionedTableDataSource.swift
//  my-cooking
//
//  Created by Julija Andriuskeviciene on 2021-09-11.
//  Copyright © 2021 ito. All rights reserved.
//

import UIKit

class SectionedTableDataSource <Element>: NSObject, UITableViewDelegate, UITableViewDataSource {
    public var dataSources: [UITableViewDataSource]

    init(dataSources: [UITableViewDataSource]) {
        self.dataSources = dataSources
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSources.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dataSource = dataSources[section]
        return dataSource.tableView(tableView, numberOfRowsInSection: 0)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataSource = dataSources[indexPath.section] as! TableDatasource<Element>
        dataSource.tableView(tableView, didSelectRowAt: indexPath)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSource = dataSources[indexPath.section]
        let indexPath = IndexPath(row: indexPath.row, section: 0)
        return dataSource.tableView(tableView, cellForRowAt: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dataSource = dataSources[section]
        return dataSource.tableView?(tableView, titleForHeaderInSection: section)
    }
}
