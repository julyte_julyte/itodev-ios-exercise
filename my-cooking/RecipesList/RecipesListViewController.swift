//
//  RecipesListViewController.swift
//  my-cooking
//
//  Created by Vladas Drejeris on 16/09/2019.
//  Copyright © 2019 ito. All rights reserved.
//

import UIKit
import AlamofireImage

class RecipesListViewController: UIViewController {

    // MARK: - UI components

    @IBOutlet private weak var tableView: UITableView!
    private var recipeButton: UIButton!

    // MARK: - Dependencies

    private let repository = RecipesRepository.shared
    private lazy var dataSources = SectionedTableDataSource<Recipe>(dataSources: [])

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        tableView.delegate = dataSources
        tableView.dataSource = dataSources
        self.createRecipeButton()
        loadData()
    }

    private func loadData() {
        for difficulty in Difficulty.allCases {
            repository.recipes(withDifficulty: difficulty) { [weak self] (result) in
                switch result {
                case .success(let recipes):
                    let recipeDataSource = makeRecipeDataSource()
                    recipeDataSource.elements = recipes
                    recipeDataSource.elementsTitle  = recipeDataSource.elements.count > 0 ? difficulty.localizedString : ""
                    dataSources.dataSources.append(recipeDataSource)
                    self?.tableView.reloadData()
                case .failure(let error):
                    self?.handleError(error)
                }
            }
        }
    }

    private func makeRecipeDataSource() -> TableDatasource<Recipe> {
        let recipeDataSource = TableDatasource<Recipe>()
        recipeDataSource.configureCell = { (element, cell) in
            cell.textLabel?.text = element.title
            cell.textLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            cell.detailTextLabel?.text = element.dificulty.localizedString
            cell.detailTextLabel?.textColor = element.dificulty.color
            cell.imageView?.contentMode = .scaleAspectFill
            cell.imageView?.clipsToBounds = true
            if let imageUrl = element.image {
                cell.imageView?.af.setImage(withURL: imageUrl,
                                            placeholderImage: UIImage(named: "placeholder_small"),
                                            filter: AspectScaledToFitSizeFilter(size: CGSize(width: 32, height: 32)))
            } else {
                cell.imageView?.image = UIImage(named: "placeholder_small")
            }
        }
        recipeDataSource.didSelectElement = { [weak self] element in
            self?.performSegue(withIdentifier: "ShowDetailsScreen", sender: element)
        }

        return recipeDataSource
    }

    private func handleError(_ error: Error) {
        // There are no errors at the moment, therefore we don't need to implement this method.
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if let recipeViewController = segue.destination as? RecipeViewController,
            let recipe = sender as? Recipe {
            recipeViewController.recipe = recipe
        }
    }

    @IBAction func unwindFromRecipeViewController(_ segue: UIStoryboardSegue) {

    }

    // MARK: - View

    func createRecipeButton() {
        self.recipeButton = UIButton()
        self.recipeButton!.setTitle("Recommend", for: .normal)
        self.recipeButton!.backgroundColor = UIColor.systemPink
        self.recipeButton?.addTarget(self, action: #selector(self.recipeButtonClicked), for: UIControl.Event.touchDown)
        self.recipeButton?.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(recipeButton!)

        // Constraints
        let margins: UILayoutGuide = self.view.layoutMarginsGuide
        self.recipeButton!.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 0).isActive = true
        self.recipeButton!.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -10).isActive = true
        self.recipeButton!.heightAnchor.constraint(equalToConstant: size(100)).isActive = true
        self.recipeButton!.widthAnchor.constraint(equalToConstant: size(300)).isActive = true
    }

    func size(_ ratio: CGFloat) -> CGFloat {
        return UIScreen.main.scale * 22 * ratio / 100
    }

    // MARK: - Actions
    @objc func recipeButtonClicked() {
        let recipeRecommendationService = RecipeRecommendationService.shared
        let element = recipeRecommendationService.getRecommendation()
        self.performSegue(withIdentifier: "ShowDetailsScreen", sender: element)
    }

}
