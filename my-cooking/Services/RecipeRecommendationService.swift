//
//  RecipeRecommendationService.swift
//  my-cooking
//
//  Created by Julija Andriuskeviciene on 2021-09-12.
//  Copyright © 2021 ito. All rights reserved.
//

/* FLOW
 1 Retrieves needed Dishes amount from DishesRepository
 
 2. If no dishes prepared or prepared < constant(minimumPrepearedDishes)
 1.1 Retrieves all recipes from RecipesRepository
 1.2 Selects random recipe
 1.3 Returns recipe for view
 
 3. If prepared dishes > constant(minimumPrepearedDishes)
 3.1 Skill level evaluated
 3.2 Retrieves recipes from RecipesRepository by evaluated difficulty
 3.3 Selects random recipe from subset
 3.4 Returns recipe
 */

class RecipeRecommendationService {

    static let shared: RecipeRecommendationService = RecipeRecommendationService()

    // MARK: - Paramethers
    let minimumPrepearedDishes: Int = 2
    //TODO: If dishesToEvaluate change need to update ranges in Difficulty maybe do this dynamicly: calculate max possible value of average and then devide to 3 ranges.
    let dishesToEvaluate: Int = 5

    // MARK: - Dependencies

    private let recipesRepository = RecipesRepository.shared
    private let dishesRepository = DishesRepository.shared

    // MARK" - State
    private lazy var dishes: [Dish] = []
    private lazy var recipes: [Recipe] = []


    //TODO: Write unit test for this func
    /// Returns recommendation of recipe according to evaluated difficulty if nothing to evaluate returns random from all
    func getRecommendation() -> Recipe{
        loadDishesData()
        if (dishes.isEmpty || dishes.count < minimumPrepearedDishes) {
            loadRecipesData()
            return getRandomRecipe()
        } else {
            let currentDifficulty  = calculatePossibleDifficulty()
            loadRecipesByDiffuculty(difficulty: currentDifficulty)
            return getRandomRecipe()
        }
    }

    // TODO: Write unit test for this func
    /// Returns calculated difficulty for recipe selection
     func calculatePossibleDifficulty() -> Difficulty {
        //Calculates avarage
        var value : Double = 0
        for dish in dishes {
            //print(" current value: \(value) \n     Dish difficulty: \(dish.recipe.dificulty.score) \n     Dish result: \(dish.result.score)")
            value += dish.recipe.dificulty.score * dish.result.score
        }
        value /= Double(dishes.capacity)
        //print("CALCULATED VALUE -----  \(value)" )

        // Finds correct range and assigns diffuculty
        var currentDifficulty = Difficulty.easy
        switch Int(value) {
        case Difficulty.easy.range:
            currentDifficulty = Difficulty.easy
        case Difficulty.normal.range:
            currentDifficulty = Difficulty.normal
        case Difficulty.hard.range:
            currentDifficulty = Difficulty.hard
        default:
            currentDifficulty  = Difficulty.easy
        }

        //print("DIFFICULTY -----  \(currentDifficulty)" )
        return currentDifficulty
    }
    
    //TODO: error handling if failed to get data (how to pass to view controller?)
    private func loadDishesData() {
        dishesRepository.lastNumberOfDishes(withNumber: dishesToEvaluate) { [weak self] (result) in
            switch result {
            case .success(let dishes):
                self?.dishes = dishes
            case .failure(let error):
                print(error.localizedDescription)
            // self?.handleError(error)
            }
        }
    }

    //TODO: error handling if failed to get data (how to pass to view controller?)
    private func loadRecipesData() {
        recipesRepository.allRecipes { [weak self] (result) in
            switch result {
            case .success(let recipes):
                self?.recipes = recipes
            case .failure(let error):
                print(error.localizedDescription)
            // self?.handleError(error)
            }
        }
    }

    //TODO: error handling if failed to get data (how to pass to view controller?)
    private func loadRecipesByDiffuculty(difficulty: Difficulty ) {
        recipesRepository.recipes(withDifficulty: difficulty) { [weak self] (result) in
            switch result {
            case .success(let recipes):
                self?.recipes = recipes
            case .failure(let error):
                print(error.localizedDescription)
            // self?.handleError(error)
            }
        }
    }

    //TODO: error handling if no recipes at all (how to pass to view controller?)
    /// Returns random recipe from loaded recipes array
    private func getRandomRecipe() -> Recipe {
        return recipes.randomElement()!
    }
}
